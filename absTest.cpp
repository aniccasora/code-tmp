# include "opencv2/opencv.hpp"


using namespace std;
using namespace cv;

// processing exception
void handle_eptr(exception_ptr eptr) // passing by value is ok
{
	try
	{
		if (eptr)
		{
			rethrow_exception(eptr);
		}
	}
	catch (const exception& e)
	{
		cout << "Caught exception \"" << e.what() << "\"\n";
	}
}

//for trackbar
Mat dst_3b_change;
Mat dst_3b;
int threshval = 100;
static void on_trackbar(int, void*)
{
	exception_ptr eptr;
	try {
		dst_3b_change = dst_3b < threshval;
		imshow("尋找最佳代換閥值", dst_3b_change);
	}
	catch (...) {
		eptr = current_exception();
	}
	handle_eptr(eptr);
}

int main() {
	
	Mat src = imread(".\\images\\cat.jpg");
	Mat src_jk = imread(".\\images\\jkcat.jpg");
	
	
	Mat src_Scharr_x, src_Scharr_y, src_Scharr_xy;
	
	int ddepth = CV_16S;
	int dtype = -1;// 輸入跟輸出 的 depth 是相同的話 就是 -1
	/*Mat sum = src*0.1 + src_jk*0.9;
	imshow("ttt", sum);*/

	// 因為一開始 的圖片是彩色的手繪圖案，會有一些 "多餘" 的 "紋路"，bilateralFilter可削去這些線條；
	// bilateralFilter(保持邊緣清晰) 他不會像 GaussianBlur 無差別模糊。
	Mat src_bilateral;
	bilateralFilter(src, src_bilateral, 101, 75, 75, BORDER_REPLICATE);
	imshow("first bilateral", src_bilateral);

	// 將 src 去噪
	Mat src_blur;
	GaussianBlur(src_bilateral, src_blur, Size(5,5),0,0,BORDER_REPLICATE);

	// 轉成灰階 以利  Scharr edge detection
	Mat src_gray;
	cvtColor(src_blur, src_gray, COLOR_BGR2GRAY);
	
	// 做 Scharr operator :x
	Scharr(src_gray, src_Scharr_x, ddepth, 1, 0);
	// 做 Scharr operator :y
	Scharr(src_gray, src_Scharr_y, ddepth, 0, 1);
	// combine dx dy
	src_Scharr_xy = src_Scharr_x + src_Scharr_y;
	imshow("src_Scharr_xy", src_Scharr_xy);

	Mat src_abs_xy;
	convertScaleAbs(src_Scharr_xy, src_abs_xy);
	imshow("src_abs_xy", src_abs_xy);
	
	// gray 做完 Scharr 以後(梯度圖)，會產生一些紋路，
	// 透過 bilateralFilter 特性將其抹去。
	Mat dst;
	bilateralFilter(src_abs_xy, dst, 9, 75, 75, BORDER_REPLICATE);
	imshow("dst", dst);

	// 由於 convertScaleAbs 轉出來的是 uchar 8-bit 通道，
	// 故需轉換成 BGR 來跟 彩色圖片做結合。
	cvtColor(dst, dst_3b,COLOR_GRAY2BGR);
	
	// 因為希望 浮水印是 黑白的(非灰階)，所以要決定怎樣程度的 "黑"才是黑，"白"才是白。
	// 故透過 滑條 去尋找 最佳閥值。
	namedWindow("尋找最佳代換閥值", WINDOW_AUTOSIZE);
	createTrackbar("Threshold", "尋找最佳代換閥值", &threshval, 255, on_trackbar);
	on_trackbar(threshval, 0);

	// try出 128的效果不錯(個人主觀)。
	Mat src_good = dst_3b < 128;
	
	// 以下就是 good(浮水印) 要跟 jk(true color RGB) 做合併~

	// 可以確保 圖片的數字 在我要的範圍  0<= value < 256
	cout << checkRange(src_jk, true, 0, 0, 256) << endl;
	cout << checkRange(src_good, true, 0, 0, 256) << endl;

	exception_ptr eptr;
	try {
		//addWeighted(src_jk, alpha, src_good, beta, 0., src_watermark);
		Mat new_size_src_jk ;
		Mat new_size_src_good;

		resize(src_jk, new_size_src_jk, Size(500, 500));
		resize(src_good, new_size_src_good, Size(500, 500));
		
		Mat src_watermark;
		src_watermark = new_size_src_jk * 0.95 + new_size_src_good * 0.05;
		imshow("src_watermark", src_watermark);
	}
	catch (...) {
		eptr = current_exception();
	}
	handle_eptr(eptr);
	
	waitKey(0);
	return 0;
}