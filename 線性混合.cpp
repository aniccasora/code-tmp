#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

Mat img1, img2, imgR;
void on_trackbar(int alpha, void *)
{
    int beta = 100 - alpha;
    addWeighted(img1, alpha * 0.01, img2, beta * 0.01, 0.0, imgR);
    //圖片應該要丟到 有建立滑條的視窗
    imshow("Blending image", imgR);
}
int main()
{
    img1 = imread("images\\lena.jpg");
    img2 = imread("images\\baboon.tiff");
    //建立 有滑條的視窗
    namedWindow("Blending image", WINDOW_AUTOSIZE);
    imshow("Image1", img1);
    imshow("Image2", img2);

    const int sliderMaxValue = 100;
    int offset = 55;

    createTrackbar("Alpha:",         //滑條名稱
                   "Blending image", //要被加入滑條的視窗
                   &offset,          //初始化閥值
                   sliderMaxValue,   //刻度範圍
                   on_trackbar);     // call back function
    on_trackbar(offset, 0);

    waitKey(0);
    return 0;
}
