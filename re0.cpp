#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
	// BGR 的 底圖
	Mat sky = imread("images\\sky.png");
	// 底圖的高度
	uint32_t h = sky.rows;
	// 底圖印的寬度
	uint32_t w = sky.cols;
	cout << "origin (w,h) => "<< w << ", " << h << endl;


	// 有透明通道的浮水印
	Mat_<Vec4b> watermark = imread("images\\cool_mark_w.png", IMREAD_UNCHANGED);

	// 浮水印的高度
	uint32_t wH = watermark.rows;
	// 浮水印的寬度
	uint32_t wW = watermark.cols;
	cout << "watermark (w,h) => " << wW << ", " << wH << endl;
	
	// 將 浮水印 的 BGR 通道拆出來 跟他的 alpha channel 做 AND Operator。

	vector<Mat> rgbaChannels(4);
	split(watermark, rgbaChannels);
	
	// blue & alpha channel
	Mat dst_blue;
	bitwise_and(rgbaChannels[0], rgbaChannels[0], dst_blue, rgbaChannels[3]);
	// green & alpha channel
	Mat dst_green;
	bitwise_and(rgbaChannels[1], rgbaChannels[1], dst_green, rgbaChannels[3]);
	// red & alpha channel
	Mat dst_red;
	bitwise_and(rgbaChannels[2], rgbaChannels[2], dst_red, rgbaChannels[3]);

	// 4個 分別為rgba 通道的 vector<Mat>
	vector<Mat> rgbANDa_Mats;
	rgbANDa_Mats.push_back(dst_blue);
	rgbANDa_Mats.push_back(dst_green);
	rgbANDa_Mats.push_back(dst_red);
	rgbANDa_Mats.push_back(rgbaChannels[3]);
	
	// 一張重新與 alpha 通道做 and 計算的 watermark。(4通道)
	Mat_<Vec4b> remask_mark;
	merge(rgbANDa_Mats, remask_mark);
	imshow("remask_mark", remask_mark);
	
	// 底圖 3通道 Vector
	vector<Mat> rgbVector_sky;
	// alpha通道的數值 應為 全255，所以先做一個 Mat，這是要給 底圖 加alpha通道用的。
	Mat ch_255(Size(sky.size()), CV_8UC1);
	ch_255.setTo(Scalar(UCHAR_MAX));

	// 調整底圖通道
	split(sky, rgbVector_sky);
	// 多加一個 alpha 通道,rgbVector_sky 此時已經是 3通道了
	rgbVector_sky.push_back(ch_255);
	// 4 channels 的 底圖
	Mat_<Vec4b> sky_bgra;
	merge(rgbVector_sky, sky_bgra);

	// 做一個 空白畫布， 讓浮水印
	Mat_<Vec4b> overlay(Size(sky_bgra.size()),CV_8UC1);
	
	remask_mark.copyTo(overlay(Rect(250,20, remask_mark.cols, remask_mark.rows)));

	// 浮水印 合成照
	Mat output;
	addWeighted(overlay, 0.25, sky_bgra,1.0,0,output);
	imshow("output", output);

	waitKey(0);
	return 0;
}