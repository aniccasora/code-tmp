#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
	Mat_<Vec4b> watermark = imread("images\\cool_mark_w.png", IMREAD_UNCHANGED);

	vector<Mat> rgbaChannels(4);
	split(watermark, rgbaChannels);

	// blue & alpha channel
	Mat dst_blue;
	bitwise_and(rgbaChannels[0], rgbaChannels[0], dst_blue, rgbaChannels[3]);
	// green & alpha channel
	Mat dst_green;
	bitwise_and(rgbaChannels[1], rgbaChannels[1], dst_green, rgbaChannels[3]);
	// red & alpha channel
	Mat dst_red;
	bitwise_and(rgbaChannels[2], rgbaChannels[2], dst_red, rgbaChannels[3]);

	// 4個 分別為rgba 通道的 vector<Mat>
	vector<Mat> rgbANDa_Mats;
	rgbANDa_Mats.push_back(dst_blue);
	rgbANDa_Mats.push_back(dst_green);
	rgbANDa_Mats.push_back(dst_red);
	rgbANDa_Mats.push_back(rgbaChannels[3]);

	// 一張重新與 alpha 通道做 and 計算的 watermark。(4通道)
	Mat_<Vec4b> remask_mark;
	merge(rgbANDa_Mats, remask_mark);
	imshow("remask_mark", remask_mark);

	imshow("watermark", watermark);

	waitKey(0);
}