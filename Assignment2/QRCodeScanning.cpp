
// QR Code Scanning
#include <iostream>
#include <opencv2\opencv.hpp>
#include "qrReader.h"
using namespace cv;
using namespace std;

// 原始圖片
Mat imgO;

double Mydot(Point2f v1, Point2f v2);
double MyVectorScalar(Point2f v1);
Point2f MyGetUnitVector(Point2f v1);
int main()
{
	vector<String> fn;
	glob(".\\images\\input\\*.jpg", fn, false);
	vector<Mat> images;
	vector<String> fn_short;
	size_t imgCount = fn.size();
	for (size_t i = 0; i < imgCount; i++) {
		// 尋找最後一個 '\' 出現的idx，用來記著檔名用。
		size_t last_slash_idx = fn[i].find_last_of("\\");

		fn_short.push_back(fn[i].substr(last_slash_idx + 1, size(fn[i])));
		images.push_back(imread(fn[i]));
	}

	for (size_t idx = 0; idx < imgCount; idx++) {
		// 灰階圖、二值化 圖片
		Mat imgGray, imgBW;
	

		imgO = images[idx];//imread("images\\input\\qr006.jpg");
		cvtColor(imgO, imgGray, COLOR_BGR2GRAY);

		adaptiveThreshold(imgGray, imgBW, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 51, 0);	
		//imshow("binary image", imgBW);
		
		qrReader reader = qrReader();
		bool found = reader.find(imgBW);
		if (found) {
			reader.drawFinders(imgO);
			cout << "QR Code detected!" << endl;

			cout << "印出 中心點 ->\t";
			for (Point2f point : reader.getPossibleCenters()) {
				cout << point << endl;
				cout << "\t\t";
			}cout << endl;

			if (reader.getPossibleCenters().size() >= 3) {
				Point2f v1, v2;
				v1.x = reader.getPossibleCenters()[1].x - reader.getPossibleCenters()[0].x;
				v1.y = reader.getPossibleCenters()[1].y - reader.getPossibleCenters()[0].y;

				v2.x = reader.getPossibleCenters()[2].x - reader.getPossibleCenters()[0].x;
				v2.y = reader.getPossibleCenters()[2].y - reader.getPossibleCenters()[0].y;

				// 調整向量 長度，因為原本是 中間到中間(短)，頂點到頂點(長)
				v1 *= 1.3;
				v2 *= 1.3;
				Point2f P4(	reader.getPossibleCenters()[0].x + v1.x + v2.x,
							reader.getPossibleCenters()[0].y + v1.y + v2.y);
				/*
				*  請當成這四方形是 歪歪 的!!
				*          ─→
				*	  	   v1
				*     P0       P1
				*      ┌───────┐
				*  ─→  │       │
				*  v2  │       │
				*      │       │
				*      └───────┘ 
				*     P2        P4(跳過3~~)
				*
				*/
				
				//這三點是 原本 QRcode 的 find pattern 的 "中心"
				Point2f P0(reader.getPossibleCenters()[0].x, reader.getPossibleCenters()[0].y);
				Point2f P1(P0 + v1);
				Point2f P2(P0 + v2);
				//      P4 在上面
				/*
				Point2f P1(reader.getPossibleCenters()[1].x, reader.getPossibleCenters()[1].y);
				Point2f P2(reader.getPossibleCenters()[2].x, reader.getPossibleCenters()[2].y);
				*/
				

				// 待校正qrCode 的中心 座標
				Point2f qrCenter((P0 + P1 + P2 + P4) / 4);
				
				// 中心 至 各點的向量
				Point2f vec_center2P0 = P0 - qrCenter;
				Point2f vec_center2P1 = P1 - qrCenter;
				Point2f vec_center2P2 = P2 - qrCenter;
				Point2f vec_center2P4 = P4 - qrCenter;
				
				// 因為 "中心" 不是我們期望的點，而是 更 邊邊一點的 角落，所以要位移一下 find pattern center points.
				double times = 1.3;
				P0 = qrCenter + vec_center2P0 * times;
				P1 = qrCenter + vec_center2P1 * times;
				P2 = qrCenter + vec_center2P2 * times;
				P4 = qrCenter + vec_center2P4 * times;
				// 已經更動 P0~P4(沒有 P3~~)
				

				/* ~~透視變換 兩點 注意 為 順時針方向~~ */

				// 原本歪歪的點(四點)
				Point2f src_points[4] = { P0,P1,P4,P2 };

				// 變成 "正方形" 的點(四點)
				Point2f dst_points[4]
					= { P0 ,
						Point2f (P0.x + MyVectorScalar(v1), P0.y),
						Point2f (P0.x + MyVectorScalar(v1), P0.y + MyVectorScalar(v2)),
						Point2f (P0.x , P0.y + MyVectorScalar(v2)) };

				// (轉換/變換)矩陣
				Mat transformation_matrix = getPerspectiveTransform(src_points, dst_points);

				// 轉換過後的圖片
				Mat afterCorrection;
				warpPerspective(imgO, afterCorrection, transformation_matrix, imgO.size());

				// 轉換過後的 QRcode 的長寬應該相等，並且 跟 v1 或 v2 的 scalar 相等
				size_t qr_wh = MyVectorScalar(v1);

				// 裁減  校正後的 QRcode
				Rect qrCode_region(dst_points[0].x, dst_points[0].y, qr_wh, qr_wh);

				// 裁減過後的 單張 正方形 QRcode
				Mat cropImage = afterCorrection(qrCode_region);
				cv::imshow("cropImage", cropImage);
			}
			else {
				cout << "Find pattern less 3!" << endl;
			}
		}
		else {
			cout << "No QR Code dected!" << endl;
		}

		imshow("Result image", imgO);
	
		cv::waitKey(0);
		cv::destroyAllWindows();
	}
	return 0;
}


double Mydot(Point2f v1, Point2f v2) {
	return	double(v1.x) * double(v2.x) +
		double(v1.y) * double(v2.y);
}

double MyVectorScalar(Point2f v1) {
	return	sqrt(pow(double(v1.x), 2) + pow(double(v1.y), 2));
}


Point2f MyGetUnitVector(Point2f v1) {
	if (sqrt(pow(v1.x, 2)) == 0) {
		return Point2f( 0,
						float(    (v1.y) / sqrt(pow(v1.y, 2))) );
	}
	else if (sqrt(pow(v1.y, 2)) == 0) {
		return Point2f(	float(    (v1.x) / sqrt(pow(v1.x, 2))  ),
						0);
	}
	else {
		return Point2f(	float(   (v1.x) / sqrt(pow(v1.x, 2))  ),
						float(   (v1.y) / sqrt(pow(v1.y, 2))  ));
	}
}