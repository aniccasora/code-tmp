# include "opencv2/opencv.hpp"
# include <iostream>
# include <cmath>
using namespace std;
using namespace cv;

int main()
{
	Mat src, dst, color_dst, color_dst2;
	src = imread("images\\123456789.png", IMREAD_GRAYSCALE);
	imshow("Source", src);
	Canny(src, dst, 100, 300, 3);
	cvtColor(dst, color_dst, COLOR_GRAY2BGR);
	color_dst.copyTo(color_dst2);
	vector<Vec2f> lines;
	HoughLines(dst, lines, 1, CV_PI / 180, 330);
	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0];
		float theta = lines[i][1];
		double a = cos(theta), b = sin(theta);
		double x0 = a * rho, y0 = b * rho;
		Point pt1(cvRound(x0 + 1000 * (-b)), cvRound(y0 + 1000 * (a)));
		Point pt2(cvRound(x0 - 1000 * (-b)), cvRound(y0 - 1000 * (a)));
		line(color_dst, pt1, pt2, Scalar(0, 0, 255), 1, 8);
	}
	imshow("Detected Lines1", color_dst);
	vector<Vec4i> lines2;
	HoughLinesP(dst, lines2, 1, CV_PI / 180, 250, 30, 5);
	for (size_t i = 0; i < lines2.size(); i++)
	{
		line(color_dst2, Point(lines2[i][0], lines2[i][1]), Point(lines2[i][2], lines2[i][3]), Scalar(0, 0, 255), 1, 8);
	}
	imshow("Detected Lines2", color_dst2);
	waitKey(0);
	return 0;
}