# include "opencv2/opencv.hpp"
# include <iostream>
# include <stdlib.h>
# include <vector>

using namespace cv;
using namespace std;


// 座標 數量 (幾個座標點，有被滑鼠標記)
uint8_t coordi_count = 0;
// 四組座標
vector<Point_<int64_t>> raw_point;
// 暫存點
Point_<int64_t> tmp_point;

// 例外處理用，code debug 用
exception_ptr eptr;
void handle_eptr(exception_ptr eptr) // passing by value is ok
{
	try
	{
		if (eptr)
		{
			rethrow_exception(eptr);
		}
	}
	catch (const exception& e)
	{
		cout << "Caught exception \"" << e.what() << "\"\n";
	}
}

void display(Mat& im, Mat& bbox)
{
	int n = bbox.rows;
	for (int i = 0; i < n; i++)
	{
		line(im, Point2i(bbox.at<float>(i, 0), bbox.at<float>(i, 1)),
			Point2i(bbox.at<float>((i + 1) % n, 0), bbox.at<float>((i + 1) % n, 1)), Scalar(0, 0, 255), 3);
	}
	imshow("Result", im);
}

void MouseCallBackFunc(int event, int x, int y, int flags, void* userdata)
{
	if (event == cv::MouseEventTypes::EVENT_LBUTTONDOWN)
	{
		//cout << "Left button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		// 當點下滑鼠左鍵時，紀錄此點
		tmp_point.x = x;
		tmp_point.y = y;
	}
}

int main(int argc, char* argv[])
{

	vector<String> fn;
	glob(".\\images\\input\\*.jpg", fn, false);
	vector<Mat> images;
	vector<String> fn_short;
	size_t count = fn.size();
	for (size_t i = 0; i < count; i++) {
		// 尋找最後一個 '\' 出現的idx，用來記著檔名用。
		size_t last_slash_idx = fn[i].find_last_of("\\");

		fn_short.push_back(fn[i].substr(last_slash_idx + 1, size(fn[i])));
		images.push_back(imread(fn[i]));
	}


	//QR檢測器
	QRCodeDetector qrDecoder = QRCodeDetector::QRCodeDetector();

	//二維碼邊框座標，提取出來的二維碼
	Mat bbox, rectifiedImage;

	// 走訪資料夾下的 圖片
	for (size_t idx=0 ;idx < images.size(); idx++)
	{
		// 全域變數 reset
		raw_point.clear();
		coordi_count = 0;

		// Read image from file 
		Mat img(images[idx]);
		imshow(fn_short[idx], img);
		
		//if fail to read the image
		if (img.empty())
		{
			cout << "Error loading the image" << endl;
			return -1;
		}

		//Create a window
		namedWindow(fn_short[idx], WINDOW_AUTOSIZE);

		//設定 滑鼠 callback function，(左鍵監聽用)
		setMouseCallback(fn_short[idx], MouseCallBackFunc, NULL);

		// 第一次執行時  tmp_point 會預設 為 0， 這樣會誤判。
		tmp_point.x = -1;
		
		// 等待 選好 QRcode
		while (true)
		{
			//show the image
			cv::imshow(fn_short[idx], img);

			if ((char)27 == (char)cv::waitKey(1)) { break; }

			if ( tmp_point.x != -1 ) {
				raw_point.push_back(Point_<int64_t>(tmp_point.x, tmp_point.y));
				cout << "讀取此 mouse 的座標 X: " << tmp_point.x << ", Y: " << tmp_point.y << endl;
				coordi_count++;
				tmp_point.x = -1;
			}
			if (coordi_count == 4) {
				cout << "已經得到四個點了" << endl;
				break;
			}
		}
		
		for (int i = 0; i < 4; i++) {
			cout << " mouse 的座標 X: " << raw_point[i].x << ", Y: " << raw_point[i].y << endl;
		}
		// 透視變換 兩點
		Point2f src_points[4] = {
			Point2l(raw_point[0].x, raw_point[0].y),
			Point2l(raw_point[1].x, raw_point[1].y),
			Point2l(raw_point[2].x, raw_point[2].y),
			Point2l(raw_point[3].x, raw_point[3].y) };

		Point2f dst_points[4] = {
			Point2l(raw_point[0].x,raw_point[0].y),
			Point2l(raw_point[2].x,raw_point[0].y),
			Point2l(raw_point[2].x,raw_point[2].y),
			Point2l(raw_point[0].x,raw_point[2].y) };
		 //原始點
		cout << raw_point[0].x << ", " << raw_point[0].y << endl;
		cout << raw_point[2].x << ", " << raw_point[0].y << endl;
		cout << raw_point[2].x << ", " << raw_point[2].y << endl;
		cout << raw_point[0].x << ", " << raw_point[2].y << endl;
		
		
		Mat transformation_matrix = getPerspectiveTransform(src_points, dst_points);
		//cout << "M: " << transformation_matrix << endl;

		Mat afterCorrection;
		warpPerspective(img, afterCorrection, transformation_matrix, img.size());

		// 選擇 一個最大的邊
		size_t qr_wh = max(
							max(
								max(norm(dst_points[0] - dst_points[1]),
									norm(dst_points[1] - dst_points[2])),
								norm(dst_points[2] - dst_points[3])),
							norm(dst_points[3] - dst_points[0]));
		
		// 裁減 qr-code
		Rect qrCode_region(dst_points[0].x, dst_points[0].y, qr_wh, qr_wh);
		
		Mat cropImage = afterCorrection(qrCode_region);
		/*
		string data = qrDecoder.detectAndDecode(cropImage, bbox, rectifiedImage);
		display(cropImage, bbox);
		cv::imshow("afterCorrection", afterCorrection);
		*/
		cv::imshow("cropImage", cropImage);
		
		cv::waitKey(0);
		destroyAllWindows();

	}
	return 0;
}



