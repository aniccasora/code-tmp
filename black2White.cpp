#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
	
	Mat_<Vec4b> input = imread("images\\cool_mark.png", IMREAD_UNCHANGED);
	imshow("before", input);
	for (int x = 0; x < input.cols; x++) {
		for (int y = 0; y < input.rows; y++) {
			if ((int)input.at<Vec4b>(x, y)[0] == 0 &&
				(int)input.at<Vec4b>(x, y)[1] == 0 &&
				(int)input.at<Vec4b>(x, y)[2] == 0 )
			{
				input.at<Vec4b>(x, y)[0] = UCHAR_MAX;
				input.at<Vec4b>(x, y)[1] = UCHAR_MAX;
				input.at<Vec4b>(x, y)[2] = UCHAR_MAX;
			}
		}
	}
	// 根本看不出來
	imshow("after", input);

	//寫至檔案
	imwrite(".\\images\\cool_mark_w.png", input);


	waitKey(0);
	return 0;
}