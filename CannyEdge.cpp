# include "opencv2/opencv.hpp"
# include <iostream>

using namespace std;
using namespace cv;

Mat src, src_gray;
Mat dst, detected_edges;
int edgeThresh = 1;
int lowThreshold=30;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 3;
void CannyThreshold(int, void*)
{
	/// Reduce noise with a kernel 3x3
	blur(src_gray, detected_edges, Size(3, 3));
	Canny(detected_edges, detected_edges, lowThreshold, lowThreshold*3, kernel_size);/// Canny detector
	dst = Scalar::all(0);/// Using Canny's output as a mask, we display our result
	src.copyTo(dst, detected_edges);
	imshow("Edge Map", dst);
}
int main()
{
	src = imread("images\\cat.jpg"); // Load an image
	imshow("Input image", src);
	dst.create(src.size(), src.type());// Create a matrix of the same type and size as src (for dst)
	cvtColor(src, src_gray, COLOR_BGR2GRAY);
	namedWindow("Edge Map", WINDOW_AUTOSIZE);
	createTrackbar("Min Threshold:", "Edge Map", &lowThreshold, max_lowThreshold, CannyThreshold);
	/// Create a Trackbar for user to enter threshold
	
	CannyThreshold(0, 0);/// Show the image
	waitKey(0);/// Wait until user exit program by pressing a key
	return 0;
}