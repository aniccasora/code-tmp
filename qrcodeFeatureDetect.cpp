#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

void display(Mat& im, Mat& bbox)
{
	int n = bbox.rows;
	for (int i = 0; i < n; i++)
	{
		line(im, Point2i(bbox.at<float>(i, 0), bbox.at<float>(i, 1)),
			Point2i(bbox.at<float>((i + 1) % n, 0), bbox.at<float>((i + 1) % n, 1)), Scalar(255, 0, 0), 3);
	}
	imshow("Result", im);
}

RNG rng(12345);

int main()
{
	
	
	//QR檢測器
	QRCodeDetector qrDecoder = QRCodeDetector::QRCodeDetector();

	//二維碼邊框座標，提取出來的二維碼
	Mat bbox, rectifiedImage;
	
	vector<String> fn;
	glob(".\\images\\input\\*.jpg", fn, false);
	vector<Mat> images;
	vector<String> fn_short;
	size_t count = fn.size(); 
	for (size_t i = 0; i < count; i++) {
		// 尋找最後一個 '\' 出現的idx，用來記著檔名用。
		size_t last_slash_idx = fn[i].find_last_of("\\");

		fn_short.push_back( fn[i].substr(last_slash_idx+1, size(fn[i])));
		images.push_back(imread(fn[i]));
	}

	// 走訪各大 images
	for (size_t idx=0; idx < size(images); idx++) {
		// 檔名 以及 圖片
		String filename = fn_short[idx];
		Mat input = Mat(images[idx]);
		
		Mat image_gray;
		cvtColor(input, image_gray, COLOR_BGR2GRAY);

		//設置角點檢測參數
		std::vector<Point2f> corners;
		int max_corners = 300;
		double quality_level = 0.4;
		double min_distance = 1;
		int block_size = 5;
		bool use_harris = false;
		double k = 0.04;

		//角點檢測
		goodFeaturesToTrack(image_gray,
			corners,
			max_corners,
			quality_level,
			min_distance,
			Mat(),
			block_size,
			use_harris,
			k);

		//將檢測到的點 繪製到 原本的圖片上
		for (int i = 0; i < corners.size(); i++)
		{
			circle(input, corners[i], 1, Scalar(0, 0, 255), 2, 8, 0);
		}

		imshow("corner", input);
		/*
		Mat edge;
		Canny(blur, edge, 100, 300, 3, true);
		imshow(filename + "edge", edge);
		
		vector<vector<Point>> contours;
		vector<Vec4i> hierarchy;
		findContours(edge, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE, Point(0, 0));

		/// Draw contours
		Mat drawing = Mat::zeros(input.size(), CV_8UC3);
		for (int i = 0; i < contours.size(); i++)
		{
			Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
			drawContours(drawing, contours, i, color, 2, 8, hierarchy, 0, Point());
		}
		imshow("Contours", drawing);
		*/
		// wait to next image~
		waitKey(0);
		destroyWindow(fn_short[idx]);
	}// 走訪各大 images 完畢!!

	
	return 0;
}