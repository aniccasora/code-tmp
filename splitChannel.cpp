#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main()
{
	Mat sky = imread("images\\Starry sky 500.png");
	
	vector<Mat> mv(4);
	split(sky, mv);

	Mat blank_ch = Mat::zeros(sky.size(), CV_8UC1);
	Mat fin_img;
	
	// for alpha default value
	Mat full_ch(sky.size(), CV_8UC1);
	full_ch.setTo(Scalar(UCHAR_MAX));
	
	vector<Mat> channels_b;
	channels_b.push_back(mv[0]);//b
	channels_b.push_back(blank_ch);
	channels_b.push_back(blank_ch);
	channels_b.push_back(full_ch);
	// 把 channels_b  的4通道 合起來。
	merge(channels_b, fin_img);
	imshow("B", fin_img);

	vector<Mat> channels_g;
	channels_g.push_back(blank_ch);
	channels_g.push_back(mv[1]);//g
	channels_g.push_back(blank_ch);
	channels_g.push_back(full_ch);
	// 把 channels_g  的4通道 合起來。
	merge(channels_g, fin_img);
	imshow("G", fin_img);

	vector<Mat> channels_r;
	channels_r.push_back(blank_ch);
	channels_r.push_back(blank_ch);
	channels_r.push_back(mv[2]);//r
	channels_r.push_back(full_ch);
	// 把 channels_r  的4通道 合起來。
	merge(channels_r, fin_img);
	imshow("R", fin_img);

	waitKey(0);
	return 0;
}