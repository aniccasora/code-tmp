#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "iostream"
#include "fstream"
#include "./aniccaUtil.cpp"

using namespace std;

int g_slider_position = 0;
//從單步mode 開始
int g_run = 1, g_dontset = 0;

cv::VideoCapture g_cap;

void onTrackbarSlide(int pos, void *) {
	g_cap.set(cv::CAP_PROP_POS_FRAMES, pos);
	if (!g_dontset)
		g_run = 1;
	g_dontset = 0;
}

int main(int argc, char ** argv) {

	cv::namedWindow("影片撥放器 DEMO", cv::WINDOW_AUTOSIZE);
	//開啟影片
	g_cap.open(string(argv[1]));
	int frames = (int)g_cap.get(cv::CAP_PROP_FRAME_COUNT );
	int tmpw = (int)g_cap.get(cv::CAP_PROP_FRAME_WIDTH );
	int tmph = (int)g_cap.get(cv::CAP_PROP_FRAME_HEIGHT);

	cout << "Vedio has " << frames << " frames of dimensions("
		<< tmpw << ", " << tmph << ")." << endl;
	
	cv::createTrackbar("Position", "影片撥放器 DEMO", &g_slider_position, frames,
		onTrackbarSlide);

	//單張
	cv::Mat frame;
	
	
	for (;;) {
		if (g_run != 0) {
			g_cap >> frame; if (frame.empty()) break;

			int current_pos = (int)g_cap.get(cv::CAP_PROP_POS_FRAMES);
			g_dontset = 1;
		
			cv::setTrackbarPos("Position", "影片撥放器 DEMO", current_pos);
			cv::resize(frame, frame, anicca::scalingImg(3,frame));
			cv::imshow("影片撥放器 DEMO", frame);
		}
	

		char c = (char)cv::waitKey(10);
		if (c=='s')// 單步
		{
			g_run = 1; cout << "Single step, run = " << g_run << endl;
		}
		if (c == 'r')//執行模式
		{
			g_run = -1; cout << "Run mode, run = " << g_run << endl;
		}
		if (c == 27)// ESC
		{
			break;
		}
	}//for end
	return 0;

}
