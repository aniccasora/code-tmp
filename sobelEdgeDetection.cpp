#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <iostream>

using namespace cv;
using namespace std;

// 負責處理 exception_ptr 傳回的訊息並印到畫面上。
exception_ptr eptr;
void handle_eptr(exception_ptr eptr) // passing by value is ok
{
	try
	{
		if (eptr)
		{
			rethrow_exception(eptr);
		}
	}
	catch (const exception& e)
	{
		cout << "Caught exception \"" << e.what() << "\"\n";
	}
}

//for trackbar
Mat dst_3b_change;
Mat dst_3b;
int threshval = 100;
static void on_trackbar(int, void*)
{
	exception_ptr eptr;
	try {
		dst_3b_change = dst_3b < threshval;
		imshow("find best threshval", dst_3b_change);
	}
	catch (...) {
		eptr = current_exception();
	}
	handle_eptr(eptr);
}

int main(int argc, char *argv[])
{
	try {
	// 原始圖案，彩色
	Mat src;
	// 高斯模糊過後的原始圖案，彩色
	Mat	src_blur;
	// blur 過後，灰階
	Mat	src_gray;
	
    
    
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;

    int c;
    
    src = imread("images\\cat.jpg");
        
    if (!src.data)
    {
        return -1;
    }

	// 平滑原圖
    GaussianBlur(src, src_blur, Size(3, 3), 0, 0, BORDER_REPLICATE);

    // Convert it to gray
    cvtColor(src_blur, src_gray, cv::COLOR_BGR2GRAY);

	// 有BGR通道的 灰階圖(後面會用到)
	Mat src_gray_bgr;
	cvtColor(src_gray, src_gray_bgr, COLOR_GRAY2BGR);

	// Scharr 算出的 X 梯度
	Mat grad_x;
	// Scharr 算出的 Y 梯度
	Mat grad_y;
	// Scharr X梯度 轉 黑白
	Mat abs_grad_x;
	// Scharr Y梯度 轉 黑白
	Mat abs_grad_y;

    // 計算 X 梯度

	// Scharr 的 結果 會有比較清楚的 "輪廓"
    Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_REPLICATE);
	// Sobel 的 "輪廓" 非常的淡 Sobel(src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_REPLICATE);
    
	// convertScaleAbs() 會將 grad_x 執行數種操作 :  縮放 -> 取絕對值 -> 換成 unsigned-8bit 資料型別。
	// 因 grad_x 被 Scharr or Sobel 處理完後 是非常不明顯的 "輪廓"。
	// 所以需要再加工 變成 黑白分明的圖案(輪廓)
	// convertScaleAbs( input, output[,α,β]); default: α=1, β=0
	// dis(i) = α * src(i) + β
    convertScaleAbs(grad_x, abs_grad_x);
		
		
    // 計算 Y 梯度
	Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
    // 將梯度 轉為 黑白(uchar)
    convertScaleAbs(grad_y, abs_grad_y);
	
    ////////// 把梯度 先相加 再轉黑白，的效果 並不好 ///////
			// !! xy梯度 相加
			Mat test = grad_y*0.5+ grad_x*0.5;
			//Mat test2Abs;
			//convertScaleAbs(test, test2Abs);
			//imshow("test2Abs", test2Abs);
	////////////////////////////////////////////////

	// 合成 xy 梯度的 輪廓圖(黑白)。
	Mat grad;

	// Total Gradient
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);
	
	// 轉成 BGR 的 輪廓圖片(after abs)，但顏色仍是 黑白!!
	Mat grad_BGR;
	// 轉換色彩 
	cvtColor(grad, grad_BGR, cv::COLOR_GRAY2BGR);
	

	//================= 將 edge detected 完畢的 圖片 再做修正 ========================
	Mat tmp_0;
	bilateralFilter(grad, tmp_0, 51, 75, 75, BORDER_REPLICATE);
	//imshow("tmp_0 bilateral", grad_bilateral);

	Mat tmp_1;
	GaussianBlur(tmp_0, tmp_1, Size(5, 5), 0, 0, BORDER_REPLICATE);
	imshow("tmp_1 GaussianBlur", tmp_1);
	//================================ 修正完畢 ====================================
	



	/////////////////////////////////////
	// 合併所有 圖片 在一個視窗
	// 2X3 圖片排列順序 左上 -> 右下

	// 分隔線寬
    int line_size = 5; // pixel
	// 視窗寬度
	int window_weight = (src.cols * 3) + (line_size * 2);
	// 視窗高度
	int window_height = (src.rows * 2) + (line_size * 1);
    //建立一個有底色的 Mat, 設定大小，以及底色。
    Mat_<Vec3b> res(Size(window_weight, window_height), Vec3b(0, 0, 0));
	
	

		// 依序填充 1.原本的圖、2.blur過後的圖, 3.Scharr 過後的圖..。
		// 1.原圖
		src.copyTo(res(Rect(0, 0, src.cols, src.rows)));
		// 2.去噪(高斯模糊)
		src_blur.copyTo(res(Rect(src.cols + line_size, 0, src_blur.cols, src_blur.rows)));
		// 3.灰階
		src_gray_bgr.copyTo(res(Rect(src.cols * 2 + line_size * 2, 0, src_gray.cols, src_gray.rows)));
		
		// 4. 空的 (梯度圖 的數值 可能落在(+-幾千內)，故無法投影在 [0,255)區間內 )
		imshow("Gradient", test);
		
		//5. 8-bit化(convertScaleAbs) 的 灰階圖片
		grad_BGR.copyTo(res(Rect( (grad_BGR.cols+ line_size), (grad_BGR.rows + line_size),
									grad.cols, grad.rows)));
		//6. 修正成有 透明圖層的 圖(也把噪點去掉的)
	
	handle_eptr(eptr);
    namedWindow("Extract outline", cv::WINDOW_AUTOSIZE);
    imshow("Extract outline", res);
    
    waitKey(0);
    return 0;
	}
	catch (...) {
		eptr = current_exception();
	}
}
