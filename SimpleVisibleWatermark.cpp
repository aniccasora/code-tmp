# include "opencv2/opencv.hpp"
# include <iostream>

using namespace std;
using namespace cv;

#define DEBUG_OPTION 0
#define output_transparency_of_watermark 0.25

// 水印 印的位置 x,y，比例([0~100],[0~100]) 會轉成 實際座標(pixel,pixel)
#define CORD_SCALE_WATERMARK_X 80
#define CORD_SCALE_WATERMARK_Y 0

// 例外處理用，code debug 用
exception_ptr eptr;
void handle_eptr(exception_ptr eptr) // passing by value is ok
{
	try
	{
		if (eptr)
		{
			rethrow_exception(eptr);
		}
	}
	catch (const exception& e)
	{
		cout << "Caught exception \"" << e.what() << "\"\n";
	}
}

//for trackbar
Mat dst_3b_change;
Mat dst_3b;
int threshval = 100;
static void on_trackbar(int, void*)
{
	exception_ptr eptr;
	try {
		dst_3b_change = dst_3b < threshval;
		imshow("find best threshval", dst_3b_change);
	}
	catch (...) {
		eptr = current_exception();
	}
	handle_eptr(eptr);
}

// 水印
Mat GLOBAL_Mat1;
// 底圖
Mat GLOBAL_Mat2;
// 輸出
Mat GLOBAL_Mat3;
// 調整 透明度用
static void on_Transparency_change(int transparency, void*) {
	double dd = (double)transparency / 100.0;
	
	addWeighted(GLOBAL_Mat1, dd, GLOBAL_Mat2, 1.0, 0, GLOBAL_Mat3);
	imshow("demo", GLOBAL_Mat3);
}

int main(int argc, char *argv[]) {
	
	// 檢查參數
	if (argc < 3) {
		cout << "Wrong input!!\nPlease add {WATERMARK} {BACKGROUND} path." << endl;
		return -1;
	}
	// 浮水印原始圖像，會被轉成 N*N 大小，建議水印的底色要是白色。
	Mat src = imread(argv[1]); //imread(".\\images\\cat.jpg");
	Mat bkg = imread(argv[2]);//imread(".\\images\\sky.png");
	
	// 檢查 檔案 是否存在
	if (src.empty() || bkg.empty()) {
		cout << "Not exists input files!" << endl;
		return -1;
	}
	/*
	// 檢查 要做成水印之圖片 是否 要補齊為 N*N。
	if (src.rows != src.cols) {
		cout << "Must resize watermark -> \"" << argv[1] << "\""<< endl;
		auto src_new_size = max(src.rows,src.cols);
		Mat resized_src(Size(src_new_size, src_new_size),CV_8UC3);
		// 全部先填入白色底色，的邊是黑色的會導致輸出會有多餘邊。
		resized_src.setTo(Scalar(255,255,255));
		// 計算 長寬差
		int offset_p = abs(src.rows - src.cols);
		if (src.rows > src.cols) {
			src.copyTo(resized_src(Rect( (int)offset_p/2, 0, src.cols,src.rows)));
		} else {
			src.copyTo(resized_src(Rect( 0, (int)offset_p/2, src.cols, src.rows)));
		}
		src = resized_src.clone();
	}
	*/
	Mat src_Scharr_x, src_Scharr_y, src_Scharr_xy;
	// Scharr 轉出的梯度數值 會到達 ABS(value) <= 4080，所以用 8bit 是無法儲存的
	int ddepth = CV_16S;
	
	// 因為一開始 的圖片是彩色的手繪圖案，會有一些 "多餘" 的 "紋路"，bilateralFilter可削去這些線條；
	// bilateralFilter(保持邊緣清晰) 他不會像 GaussianBlur 無差別模糊。
	Mat src_bilateral;
	bilateralFilter(src, src_bilateral, 9, 75, 75, BORDER_REPLICATE);
	//imshow("first bilateral", src_bilateral);
	
	// 經過GaussianBlur 的浮水印原始圖案
	Mat src_blur;
	GaussianBlur(src_bilateral, src_blur, Size(5,5),0,0,BORDER_REPLICATE);

	// 轉成灰階 以利  Scharr edge detection
	Mat src_gray;
	cvtColor(src_blur, src_gray, COLOR_BGR2GRAY);
	
	// 做 Scharr operator :dx
	Scharr(src_gray, src_Scharr_x, ddepth, 1, 0);
	// 做 Scharr operator :dy
	Scharr(src_gray, src_Scharr_y, ddepth, 0, 1);
	
	// 梯度 轉 [0,255]區間 邊縣圖，(gray channel)。
	Mat src_Scharr_x_abs, src_Scharr_y_abs;
	convertScaleAbs(src_Scharr_x, src_Scharr_x_abs);
	convertScaleAbs(src_Scharr_y, src_Scharr_y_abs);

	// dx, dy梯度的 gray邊線圖 線性疊加。
	Mat src_Scharr_xPy_abs;
	addWeighted(src_Scharr_x_abs, 0.5, src_Scharr_y_abs, 0.5, 0, src_Scharr_xPy_abs);
	//imshow("src_Scharr_xPy_abs", src_Scharr_xPy_abs);
	
	// 做完 Scharr 以後，邊線圖 會產生一些紋路，
	// 透過 bilateralFilter 特性將其抹去。
	Mat second_bilateralFilter;
	bilateralFilter(src_Scharr_xPy_abs, second_bilateralFilter, 9, 75, 75, BORDER_REPLICATE);
	//imshow("second_bilateralFilter", second_bilateralFilter);
	
	// 由於 convertScaleAbs 轉出來的是 uchar 8-bit 通道，
	// 故需轉換成 BGR 來跟 彩色圖片做結合。
	cvtColor(second_bilateralFilter, dst_3b,COLOR_GRAY2BGR);
	//imshow("dst_3b", dst_3b);
	

	// 因為希望 浮水印是 黑白的(非灰階)，所以要決定怎樣程度的 "黑"才是黑，"白"才是白。
	// 故透過 滑條 去尋找 最佳閥值。
	if (DEBUG_OPTION) {
		namedWindow("find best threshval", WINDOW_AUTOSIZE);
		createTrackbar("Threshold", "find best threshval", &threshval, 255, on_trackbar);
		on_trackbar(threshval, 0);
	}

	// try出 60的效果不錯(個人主觀)。
	int best_threshval = 60;
	Mat src_good = dst_3b < best_threshval;
	//imshow("src_good", src_good);
	
	//================= 將 src_good 再做修正 ========================

	Mat tmp_0;
	bilateralFilter(src_good, tmp_0, 9, 75, 75, BORDER_REPLICATE);
	//imshow("tmp_0 bilateralFilter", tmp_0);

	Mat src_best;
	bilateralFilter(tmp_0, src_best, 9, 75, 75, BORDER_REPLICATE);
	//imshow("src_best", src_best);
	
	//======================= 修正完畢 ==============================

	// 目前 浮水印是 白底黑線, 3 channels
	//============ 增加 alpha 通道，讓白像素的 alpha 為 0xff ===========

	Mat_<Vec4b> src_best_bgra;
	cvtColor(src_best, src_best_bgra, COLOR_BGR2BGRA);
	
	// 拆成 vector 的  浮水印
	vector<Mat> vectorOf_bgra;
	split(src_best_bgra, vectorOf_bgra);
	
	Mat b = vectorOf_bgra[0];
	Mat g = vectorOf_bgra[1];
	Mat r = vectorOf_bgra[2];
	Mat alpha = vectorOf_bgra[3];
	
	for (int  row = 0; row < src_best_bgra.rows; ++row) {
		for (int col = 0; col < src_best_bgra.cols; ++col) {
			try {
				// 如果 該 pixel 是白色的 就把 alpha 通道設為 0x00;
				if ( b.at<uchar>(row, col) & g.at<uchar>(row, col) & r.at<uchar>(row, col) & 0xff ) {
					alpha.at<uchar>(row, col) = 0x00;
				}
			}
			catch (...) {
				eptr = current_exception();
				cout << "(row, col) = " << row << ", " << col << endl;
				system("pause");
			}
			handle_eptr(eptr);
		}
	}
	// 合併 調整過後 的 bgra 通道
	merge(vectorOf_bgra, src_best_bgra);
	// 一樣是"白底"黑線 (存成png後 就是透明),四通道!!
	//imshow("src_best_bgra", src_best_bgra);
	// 儲存 watermark~
	imwrite(".\\images\\watermark_black.png", src_best_bgra);
	//waitKey(0);
	//======================= 增加 alpha 已完成 ========================

	//======================= 黑邊轉白邊 ==============================
	// 拆成 vector 的  浮水印 (白邊線), vectorOf_bgra -> 黑邊線
	vector<Mat> vectorOf_bgra_white(vectorOf_bgra);

	// 只需要管 黑色的邊就好， 把所有黑色的邊都轉作 0xff
	for (int row = 0; row < src_best_bgra.rows; ++row) {
		for (int col = 0; col < src_best_bgra.cols; ++col) {
			if (( !vectorOf_bgra_white[0].at<uchar>(row, col)&
				  !vectorOf_bgra_white[1].at<uchar>(row, col)&
				  !vectorOf_bgra_white[2].at<uchar>(row, col)) & 0xff) {
				vectorOf_bgra_white[0].at<uchar>(row, col) = 0xff;
				vectorOf_bgra_white[1].at<uchar>(row, col) = 0xff;
				vectorOf_bgra_white[2].at<uchar>(row, col) = 0xff;
			}
				
		}
	}

	Mat_<Vec4b> watermark_white;
	merge(vectorOf_bgra_white, watermark_white);
	
	// 此圖示看不出來 因為是全白的
	//imshow("after", watermark_white);
	//waitKey(0);
	// 所以寫至檔案
	imwrite(".\\images\\watermark_white.png", watermark_white);
	
	//======================= 黑邊轉白邊完畢 ============================

	//============== 以下 將轉換完成的 watermark 印在底圖上================
	// 底圖的高度
	uint32_t h = bkg.rows;
	// 底圖印的寬度
	uint32_t w = bkg.cols;
	cout << "background image (w,h) => " << w << ", " << h << endl;

	// 有透明通道的浮水印
	Mat_<Vec4b> watermark = imread("images\\watermark_white.png", IMREAD_UNCHANGED);

	// 浮水印的高度
	uint32_t wH = watermark.rows;
	// 浮水印的寬度
	uint32_t wW = watermark.cols;
	cout << "watermark (w,h) => " << wW << ", " << wH << endl;

	// 浮水印的極限 座標，超出去的話會發生例外。
	if (h < wH || w < wW) {
		cout << "background (width or hight) < watermark Max(width, hight)" << endl;
		return -1;
	}
	// include this value
	uint32_t limit_cord_watermark_x = w - wW;
	uint32_t limit_cord_watermark_y = h - wH;

	// 將 浮水印 的 BGR 通道拆出來 跟他的 alpha channel 做 AND Operator。

	vector<Mat> rgbaChannels(4);
	split(watermark, rgbaChannels);

	// blue & alpha channel
	Mat dst_blue;
	bitwise_and(rgbaChannels[0], rgbaChannels[0], dst_blue, rgbaChannels[3]);
	// green & alpha channel
	Mat dst_green;
	bitwise_and(rgbaChannels[1], rgbaChannels[1], dst_green, rgbaChannels[3]);
	// red & alpha channel
	Mat dst_red;
	bitwise_and(rgbaChannels[2], rgbaChannels[2], dst_red, rgbaChannels[3]);

	// 4個 分別為rgba 通道的 vector<Mat>
	vector<Mat> rgbANDa_Mats;
	rgbANDa_Mats.push_back(dst_blue);
	rgbANDa_Mats.push_back(dst_green);
	rgbANDa_Mats.push_back(dst_red);
	rgbANDa_Mats.push_back(rgbaChannels[3]);

	// 一張重新與 alpha 通道做 and 計算的 watermark。(4通道)
	Mat_<Vec4b> remask_mark;
	merge(rgbANDa_Mats, remask_mark);
	//imshow("remask_mark", remask_mark);

	// 底圖 3通道 Vector
	vector<Mat> rgbVector_sky;
	// alpha通道的數值 應為 全255，所以先做一個 Mat，這是要給 底圖 加alpha通道用的。
	Mat ch_255(Size(bkg.size()), CV_8UC1);
	ch_255.setTo(Scalar(UCHAR_MAX));

	// 調整底圖通道
	split(bkg, rgbVector_sky);
	// 多加一個 alpha 通道,rgbVector_sky 此時已經是 3通道了
	rgbVector_sky.push_back(ch_255);
	// 4 channels 的 底圖
	Mat_<Vec4b> sky_bgra;
	merge(rgbVector_sky, sky_bgra);

	// 做一個 空白畫布， 將浮水印放上去 (這邊用到CV_8UC1，也意外會過!)
	Mat_<Vec4b> overlay(Size(sky_bgra.size()), CV_8UC3);
	

	int scale_x = CORD_SCALE_WATERMARK_X;
	int scale_y = CORD_SCALE_WATERMARK_Y;
	// 如果 有在 command line給 x 比率座標
	if (argc > 4) {
		cout << "\ncatch input >> scale coordinate_X=" << argv[4] << endl;
		scale_x = atof(argv[4]);
	}
	// 如果 有在 command line給 y 比率座標
	if (argc > 5) {
		cout << "catch input >> scale coordinate_Y=" << argv[5] << endl << endl;
		scale_y = atof(argv[5]);
	}
	//水實際印出位置
	int print_coordinate_x = limit_cord_watermark_x * ((double)scale_x / 100.0);
	int print_coordinate_y = limit_cord_watermark_y * ((double)scale_y / 100.0);
	cout << "Actually printed position : "<< print_coordinate_x << ", " << print_coordinate_y << "\n\n";
	remask_mark.copyTo(overlay(Rect(print_coordinate_x, print_coordinate_y, remask_mark.cols, remask_mark.rows)));

	Mat output;
	
	// Setting GLOBAL variable
	GLOBAL_Mat1 = overlay.clone();
	GLOBAL_Mat2 = sky_bgra.clone();
	// 初始化透明度
	int transparency = 25;

	namedWindow("demo", WINDOW_NORMAL);
	createTrackbar("Alpha", "demo", &transparency, 100, on_Transparency_change);
	on_Transparency_change(transparency, 0);
	
	// defalut output is 0.25，如有指定 command line argument 的話就會被替換掉。
	float tpc = output_transparency_of_watermark;
	// 如果 有給 透明度
	if (argc > 3) {
		cout << "catch input >> transparency=" << argv[3] << endl;
		tpc = atof(argv[3]);
	}
	
	addWeighted(overlay, tpc, sky_bgra, 1.0, 0, output);
	imshow("output", output);
	imwrite("images\\output.png", output);
	waitKey(0);
	return 0;
}