#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"
namespace anicca
{

	std::string getFilename(std::string path)
	{
		return path.substr(path.rfind('\\') + 1, path.size());
	}

	cv::Size scalingImg(int Zoom_out_times, cv::Mat img)
	{
		return cv::Size_<int>(img.size().width / Zoom_out_times,
							  img.size().height / Zoom_out_times);
	}

	// 負責處理 exception_ptr 傳回的訊息並印到畫面上。
	void handle_eptr(std::exception_ptr eptr) // passing by value is ok
	{
		try
		{
			if (eptr)
			{
				std::rethrow_exception(eptr);
			}
		}
		catch (const std::exception& e)
		{
			std::cout << "Caught exception \"" << e.what() << "\"\n";
		}
	}

} // namespace anicca
