# include "opencv2/opencv.hpp"


using namespace std;
using namespace cv;

// processing exception

exception_ptr eptr;
void handle_eptr(exception_ptr eptr) // passing by value is ok
{
	try
	{
		if (eptr)
		{
			rethrow_exception(eptr);
		}
	}
	catch (const exception& e)
	{
		cout << "Caught exception \"" << e.what() << "\"\n";
	}
}

int main() {

	//浮水印
	Mat_<Vec4b> mark = imread("images\\cool_mark_w.png", IMREAD_UNCHANGED);// 原圖本身就有 alpha
	//星空
	Mat_<Vec4b> sky;
	cvtColor(imread("images\\Starry sky 500.png", IMREAD_COLOR), sky,COLOR_BGR2BGRA);// BGR

	// 去掉點點的浮水印
	Mat mark_cool;
	//轉成GRAY 的 mark
	Mat mark_1c;
	
	cvtColor(mark, mark_1c, COLOR_BGRA2GRAY);
	GaussianBlur(mark_1c, mark_cool, Size(9, 9), 0, 0, BORDER_REPLICATE);

	// 去掉噪點的 mark
	Mat mark_nn;
	bilateralFilter(mark_cool, mark_nn, 51, 75, 75, BORDER_REPLICATE);

	/////////////// 修改 mark_nn 的 alpha channel ///////////////////

	// 擁有 alpha channel 的去噪 mark
	Mat mark_nn_bgra;
	cvtColor(mark_nn>128, mark_nn_bgra,COLOR_GRAY2BGRA);
	
	for (int x = 0; x < mark_nn_bgra.rows; x++) {
		for (int y = 0; y < mark_nn_bgra.cols; y++) {
			if( (int)mark_nn_bgra.at<Vec4b>(x, y)[0] == UCHAR_MAX && //原本 (int)mark_nn_bgra.at<Vec4b>(x, y)[1]
				(int)mark_nn_bgra.at<Vec4b>(x, y)[1] == UCHAR_MAX &&
				(int)mark_nn_bgra.at<Vec4b>(x, y)[2] == UCHAR_MAX)
			{
				mark_nn_bgra.at<Vec4b>(x, y)[3] = 0;
			}
		}
	}
	

	// 3通道 混合 alpha
	vector<Mat> mv(4),bbbb;
	split(mark_nn_bgra, mv);
	// 把 mark 拆 4 channel
	Mat mark_B(mv[0]);
	Mat mark_G(mv[1]);
	Mat mark_R(mv[2]);
	Mat mark_alpha(mv[3]);

	Mat mix_B, mix_G, mix_R;
	bitwise_and(mark_B, mark_alpha, mix_B);
	bitwise_and(mark_G, mark_alpha, mix_G);
	bitwise_and(mark_R, mark_alpha, mix_R);

	vector<Mat> mixBGRA;
	mixBGRA.push_back(mark_B);
	mixBGRA.push_back(mark_G);
	mixBGRA.push_back(mark_R);
	mixBGRA.push_back(mark_alpha);

	Mat_<Vec4b> watermark;
	merge(mixBGRA, watermark);
	imshow("watermark", watermark);
	cout << watermark.channels() << endl;
	cout << sky.channels() << endl;
	cout << watermark.size() << endl;
	cout << sky.size() << endl;
	
	imwrite("images\\cool_mark.png", mark_nn_bgra);

	double alpha = 1.0;
	Mat_<Vec4b> result;

	cout << result.channels() << endl;

	for (double i = 1; i >= 0; i -=0.1) {
		double tmp = alpha - i;
		try {
			addWeighted(sky, tmp, watermark, 1.0 - tmp, 0, result);
			imshow("result", result);
		}
		catch (...) {
			eptr = current_exception();
		}
		handle_eptr(eptr);
		waitKey(500);
	}
}
